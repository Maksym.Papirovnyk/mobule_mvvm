import React, {
    useReducer, createContext,
} from 'react';

export const initialState = {
    vehicles: {
        total: 0
    }
};

function reducer(state, action) {
    switch (action.type) {
        case 'setVehiclesData':
            return {
                ...state,
                vehicles: {
                    ...state.vehicles,
                    ...action.payload }
            };
        default:
            return state;
    }
}

const AppContext = createContext(initialState);

function AppProvider({ children }) {
    const [globalState, dispatch] = useReducer(reducer, initialState);

    return <AppContext.Provider value={{ globalState, dispatch }}>{children}</AppContext.Provider>;
}

export { AppContext, AppProvider };
