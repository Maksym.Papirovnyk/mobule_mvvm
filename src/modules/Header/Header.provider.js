import {useContext} from 'react';
import {AppContext} from "../../context/appState.context";

const HeaderProvider = () => {
    const { globalState } = useContext(AppContext);
    const { total } = globalState.vehicles;

    return (
        <div>
            you selected {total} vehicles
        </div>
    );
};

export default HeaderProvider;
