import { useMemo } from "react";
import './styles/list.styles.css'

export const List = ({data}) => useMemo(() => {
    const isEmpty = Boolean(data.length);

    return (
        <div className="list">

            <h2>
                {isEmpty ? (<span>your list</span>) : (<span>please add a car</span>)}
            </h2>

            <ol>
                {data.map(({id ,name}) => (<li key={id}>{name}</li>))}
            </ol>
        </div>
    )
}, [data]);
