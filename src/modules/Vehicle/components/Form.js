import React from 'react';
import {Input} from "./Input";
import './styles/form.styles.css';

const Form = ({handleSubmit, inputValue, handleChange}) => {
    return (
        <form className="form_main" onSubmit={handleSubmit}>
            <Input
                text={inputValue}
                handleChange={handleChange}
            />
        </form>
    );
};

export default Form;
