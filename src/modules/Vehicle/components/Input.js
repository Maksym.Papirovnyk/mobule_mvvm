import './styles/input.syles.css';

export const Input = ({ text,  handleChange}) => {
    return (
        <input
            type="text"
            value={text}
            className="input_main"
            onChange={handleChange}
        />)
};
