import {VehicleController} from "./Vehicle.controller";
import {VehicleView} from './Vehicle.view'

export const VehicleProvider = () => {
    return (
        <VehicleController View={VehicleView} />
    )
}
