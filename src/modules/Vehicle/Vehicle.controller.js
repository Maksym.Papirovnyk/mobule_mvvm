import {useState, useReducer, useContext, useEffect} from "react";
import {ACTIONS, initialState, reducer} from './Vehicle.viewModel'
import {AppContext} from "../../context/appState.context";

export const VehicleController = ({ View }) => {
    const { dispatch: globalStateDispatch } = useContext(AppContext);
    const [vehicles, dispatch] = useReducer(reducer, initialState);
    const [name, setName] = useState('')

    const handleSubmit = (e) => {
        e.preventDefault();

        dispatch({
            type: ACTIONS.ADD_TODO,
            payload: { name }
        })

        setName('');
    }

    useEffect(() => {
        globalStateDispatch({
            type: 'setVehiclesData',
            payload: {
                total: vehicles.length
            },
        });
    }, [vehicles])

    const handleChange = (e) => setName(e?.target?.value);

    return (
        <>
            <View
                state={vehicles}
                inputValue={name}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
            />
        </>)
};
