import {addTodo} from "./reducers/addTodo";

export const initialState = [];

export const ACTIONS = {
    ADD_TODO: 'add-todo',
    REMOVE_TODO: 'remove-todo'
};

export const reducer = (state, action) => {
    switch (action.type) {
        case ACTIONS.ADD_TODO: {
            return addTodo(state, action.payload.name)
        }
        default: {
            return state
        }
    }
}
