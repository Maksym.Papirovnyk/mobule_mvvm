export const addTodo = (state, name) => {
    return [
        ...state,
        {
            name,
            id: Date.now(),
            complete: false
        }
    ]
}
