import Form from "./components/Form";
import { List } from "./components/List";

export const VehicleView = ({
    state,
    inputValue,
    handleSubmit,
    handleChange
}) => {
    return (
        <div>
            <h1>Enter vehicle</h1>

            <Form
                inputValue={inputValue}
                handleSubmit={handleSubmit}
                handleChange={handleChange}
            />

            <List data={state} />
        </div>
    )
};
