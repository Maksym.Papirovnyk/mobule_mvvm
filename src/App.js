import { VehicleProvider } from './modules/Vehicle/Vehicle.provider'
import HeaderProvider from "./modules/Header/Header.provider";

function App() {
  return (
    <div>
        <HeaderProvider />
        <VehicleProvider />
    </div>
  );
}

export default App;
